package com.sambaonibus.samba_onibus.Util;

import java.util.Random;

public class GeradorCidadePassageiro {

    String city = "";
    public String geraCidadePassageiro(){

        Random rand = new Random();
        int indicador = rand.nextInt(100);

        if (indicador < 40){
            city = "Pelotas";
        } else if (indicador >= 40 && indicador < 60){
            city = "Rio Grande";
        } else if (indicador >= 60 && indicador < 65){
            city = "Porto Alegre";
        } else if (indicador >= 65 && indicador < 75){
            city = "Camaquã";
        } else if (indicador >= 75 && indicador < 80){
            city = "Canguçu";
        } else if (indicador >= 80 && indicador < 85){
            city = "Arroio do Padre";
        } else if (indicador >= 85 && indicador < 90){
            city = "Jaguarão";
        } else if (indicador >= 90 && indicador < 95){
            city = "Santana da Boa Vista";
        } else if (indicador >= 95 && indicador <= 100){
            city = "Santa Maria";
        }
        return city;
    }



    String cidade = geraCidadePassageiro();
    @Override
    public String toString() {
        return cidade;
    }

}
