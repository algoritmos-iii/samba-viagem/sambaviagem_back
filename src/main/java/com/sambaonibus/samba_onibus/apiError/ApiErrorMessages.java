package com.sambaonibus.samba_onibus.apiError;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ApiErrorMessages {


    private HttpStatus status;
    private String mensagem;
    private int statusCode;



    public ApiErrorMessages(String mensagem) throws IOException {
        this.status = getStatus();
        this.mensagem = mensagem;
        //this.statusCode = "teste";

        ClientHttpResponse response = null;
        this.statusCode = response.getRawStatusCode();
    }

    public String mensagemErro(){
    return getMessage();
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return mensagem;
    }

    public void setMessage(String mensagem) {
        this.mensagem = mensagem;
    }

    public int getStatusCode() {
        return statusCode;
    }
}

