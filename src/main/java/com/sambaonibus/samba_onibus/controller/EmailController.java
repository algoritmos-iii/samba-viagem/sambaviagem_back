package com.sambaonibus.samba_onibus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.internet.MimeMessage;

@RestController
public class EmailController {

    @Autowired private JavaMailSender mailSender;

    @RequestMapping(path = "/email-send", method = RequestMethod.GET)
    public String sendMail() {
        try {
            MimeMessage mail = mailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(mail);
            helper.setTo( "mmendesk@icloud.com" );
            helper.setSubject( "Samba Onibus - Aluguel e Excurções" );
            helper.setText(
                    "<p>A nossa empresa está sempre em busca por oferecer serviços cada vez mais completos e eficientes, o SambaOnibus " +
                            "mantém alianças com os principais fornecedores e agências especializadas nos mais variados ramos " +
                            "do turismo, de negócios a lazer.</p>" + "<p>Venha viajar conosco, confira o que nós temos a oferecer:</p>" +
                            "<p>- São diversos ônibus disponíveis para locação com destinos diversos, inclusive nos " +
                            "países pertencentes ao Mercosul. </p>" +
                            "<p>- Oferecem o que há de melhor e mais confortável e, dependendo do " +
                            "modelo, podem contar com A/C, geladeira, forno, bebedouro e tela LCD com DVD.</p>" +
                            "<p>- Segurança e conforto</p>" +
                            "\n" + "\n" + "<b>''O sucesso profissional é sonho, Turismo é a realização!!</b>''" + "\n" + "\n" +
                            "<p>Fone: (53) 98998-2595</p>" +
                            "<p>Endereço: Avenida Fernando Osório, 2093</p>", true);
            mailSender.send(mail);

            return "E-mail enviado com sucesso!";
        } catch (Exception e) {
            e.printStackTrace();
            return "Erro ao enviar e-mail";
        }
    }
}