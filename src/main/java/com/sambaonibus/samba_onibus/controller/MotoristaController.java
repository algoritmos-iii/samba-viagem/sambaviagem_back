package com.sambaonibus.samba_onibus.controller;

import com.sambaonibus.samba_onibus.apiError.ApiErrorMessages;
import com.sambaonibus.samba_onibus.domain.Motorista;
import com.sambaonibus.samba_onibus.repository.MotoristaRepository;
import org.flywaydb.core.internal.util.ClassUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.persistence.Access;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/motoristas")

//Indica o tratamento de exceções que irá dentro da classe
@ControllerAdvice


public class MotoristaController {

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request) throws IOException {

        String mensagemErroDesc = ex.getLocalizedMessage();
        if (mensagemErroDesc == null) mensagemErroDesc = ex.toString();

        ApiErrorMessages mensagemErro = new ApiErrorMessages(mensagemErroDesc);






        return new ResponseEntity<>(
                mensagemErro, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @Autowired
    private MotoristaRepository motoristaObj;


    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_PESQUISAR_MOTORISTA')")
    public List<Motorista> listar() {
        return motoristaObj.findAll();
    }



    @CrossOrigin
    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_CADASTRAR_MOTORISTA')")
    public ResponseEntity<Motorista> salvar(@Valid @RequestBody Motorista motorista) {
        Motorista motora = motoristaObj.save(motorista);
        return new ResponseEntity<Motorista>(motora, HttpStatus.CREATED);
    }

    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_PESQUISAR_MOTORISTA')")
    //@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Ops, por algum motivo não foi possível processar a requisição.")
    //void onIllegalArgumentException(IllegalArgumentException exception) {}


    public Motorista buscar(@PathVariable int id) {
        return motoristaObj.findById(id).get();
    }

    @CrossOrigin
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_REMOVER_MOTORISTA')")
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Ops, por algum motivo não foi possível processar a requisição.")
    public void remover(@PathVariable int id) {
        Motorista motorista = motoristaObj.findById(id).get();
        motoristaObj.delete(motorista);
    }

    @CrossOrigin
    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_CADASTRAR_MOTORISTA')")
    public ResponseEntity<Motorista> editar(@Valid @RequestBody Motorista motoristaParametro) {
        Motorista motorista = motoristaObj.save(motoristaParametro);
        return new ResponseEntity<Motorista>(motorista, HttpStatus.OK);

    }
}
