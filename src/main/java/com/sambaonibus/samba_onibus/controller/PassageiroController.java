package com.sambaonibus.samba_onibus.controller;

import com.sambaonibus.samba_onibus.Util.*;
import com.sambaonibus.samba_onibus.domain.Passageiro;
import com.sambaonibus.samba_onibus.repository.PassageiroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/passageiros")

public class PassageiroController {

    @Autowired
    private PassageiroRepository passageiroObj;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_PESQUISAR_PASSAGEIRO_VALIDO')")
    public List<Passageiro> listValidos() {
        return passageiroObj.findAll();
        
    }

    @Transactional
    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_CADASTRAR_PASSAGEIRO')")
    public ResponseEntity<Passageiro> save(@Valid @RequestBody Passageiro pass) {
        Passageiro passageiro = passageiroObj.save(pass);
        return new ResponseEntity<Passageiro>(passageiro, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_PESQUISAR_PASSAGEIRO')")
    public Passageiro send(@PathVariable int id) {

        final Passageiro passageiro = passageiroObj.findById(id).get();
        return passageiro;
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_REMOVER_PASSAGEIRO')")
    public void remove(@PathVariable int id) {
        Passageiro passageiro = passageiroObj.findById(id).get();
        passageiroObj.delete(passageiro);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_CADASTRAR_PASSAGEIRO')")
    public ResponseEntity<Passageiro> edit( @RequestBody Passageiro passageiroParametro) {
        Passageiro passageiro = passageiroObj.save(passageiroParametro);
        return new ResponseEntity<Passageiro>(passageiro, HttpStatus.OK);

    }


    @Scheduled(cron = "0 */01 * ? * *")
    public void scheduleFixedDelayTask() {


        //Gera o nome e os sobrenomes
        Nomes nome = new Nomes();
        Sobrenomes sobrenome1 = new Sobrenomes();
        Sobrenomes sobrenome2 = new Sobrenomes();
        String nomeCompleto = nome +" "+ sobrenome1+" "+sobrenome2;

        // Gera o CPF:
        GeradorCPF cpf = new GeradorCPF();
        String cpfStr = cpf.toString();

        // Gera o RG:
        GeradorRg rg = new GeradorRg();
        String rgStr = rg.toString();

        // Gera o Telefone:
        GeradorTelefone telefone = new GeradorTelefone();
        String telStr = telefone.toString();

        // Gera o Endereço:
        GeradorEndereco endereco = new GeradorEndereco();
        String endStr = endereco.toString();

        // Gera a Cidade:
        GeradorCidadePassageiro cdPas = new GeradorCidadePassageiro();
        String cdPasStr = cdPas.toString();


        //Gera a data de nascimento
        GeradorDataMaior data = new GeradorDataMaior();
        //Converte data para String
        String dataStr = data.toString();
        //Converte String para o formato de Data
        LocalDate localDate = LocalDate.parse(dataStr);

        //Lista Vazia
        List myList = new ArrayList();

        Date date= new Date();
        long time = date.getTime();
        Timestamp ts = new Timestamp(time);

        Timestamp nulo = new Timestamp(0000000000000000000000000000000);


        //Cria um objetoPassageiro com os dados gerados anteriormente
        Passageiro passageiroObjeto = new Passageiro();
        //nomeCompleto, cpfStr, rgStr, localDate,
        //        telStr, endStr, cdPasStr
        passageiroObjeto.setNome(nomeCompleto);
        passageiroObjeto.setCpf(cpfStr);
        passageiroObjeto.setRg(rgStr);
        passageiroObjeto.setNascimento(localDate);
        passageiroObjeto.setTelefone(telStr);
        passageiroObjeto.setEndereco(endStr);
        passageiroObjeto.setCidade(cdPasStr);
        passageiroObjeto.setCreated_at(ts);
        passageiroObjeto.setUpdated_at(ts);
        passageiroObjeto.setDeleted(false);


        //Insere o passageiro no banco de dados
        Passageiro passageiro = passageiroObj.save(passageiroObjeto);


        System.out.println("------ PASSAGEIRO REGISTRADO COM SUCESSO ------");
        System.out.println("- Nome do passageiro: "+nomeCompleto+" --- "+LocalDateTime.now());
    }
}
