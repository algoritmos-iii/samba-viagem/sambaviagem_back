package com.sambaonibus.samba_onibus.controller;

import com.sambaonibus.samba_onibus.domain.Viagem;
import com.sambaonibus.samba_onibus.repository.ViagensRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/viagens")

public class ViagensController {

    @Autowired
    private ViagensRepository viagemObj;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_PESQUISAR_VIAGEM')")
    public List<Viagem> list() {
        return viagemObj.findAll();
    }


    @Transactional
    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_CADASTRAR_VIAGEM')")
    public ResponseEntity<Viagem> save (@Valid @RequestBody Viagem viageM) {
        Viagem viagem = viagemObj.save(viageM);
        return new ResponseEntity<Viagem>(viagem, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_PESQUISAR_VIAGEM')")
    public Viagem send(@PathVariable int id) {

        final Viagem viagem = viagemObj.findById(id).get();
        return viagem;
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_REMOVER_VIAGEM')")
    public void remove(@PathVariable int id) {
        Viagem viagem = viagemObj.findById(id).get();
        viagemObj.delete(viagem);
    }

    @PutMapping
    @PreAuthorize("hasAnyRole('ROLE_CADASTRAR_VIAGEM')")
    public ResponseEntity<Viagem> edit( @RequestBody Viagem viagemParametro) {
        Viagem viagem = viagemObj.save(viagemParametro);
        return new ResponseEntity<Viagem>(viagem, HttpStatus.OK);

    }
}
