package com.sambaonibus.samba_onibus.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity(name = "viagem")
public @Data
class Viagem {

    //TODO: Criar relacionamento das tabelas
    //TODO: Criar um COUNT nos passageiros para calcular o número de passageiros da viagem.

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToMany(targetEntity = Passageiro.class)
    @JoinTable(name="viagem_passageiros",
            joinColumns= {@JoinColumn(name="viagem_id")},
            inverseJoinColumns= {@JoinColumn(name="passageiro_id")}
    )
    private List<Passageiro> passageiros;

    @ManyToMany(targetEntity = Motorista.class)
    @JoinTable(name="viagem_motoristas",
            joinColumns= {@JoinColumn(name="viagem_id")},
            inverseJoinColumns= {@JoinColumn(name="motorista_id")}
            )
    private List<Motorista> motoristas;

    @Column()
    private double dist_pre;

    @Column()
    private double dist_real;

    @NotNull(message = "Origem da viagem inválido")
    @Column(nullable = false)
    private String origem;

    @NotNull(message = "Origem da viagem inválido")
    @Column(nullable = false)
    private int origem_estado;

    @NotBlank(message = "Destino de viagem inválido")
    @Column(nullable = false)
    private String destino;

    @NotNull(message = "Origem da viagem inválido")
    @Column(nullable = false)
    private int destino_estado;

    @Column()
    private double orcamento;

    //@NotBlank(message = "Data de saída inválida")
    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate data_s;

    //@NotBlank(message = "Data de saída inválida")
    @Column(nullable = false)
    private Time hora_s;

    //@NotBlank(message = "Data de retorno inválido")
    @Column(nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate data_r;

    @Column()
    private Timestamp created_at;

    @Column()
    private Timestamp updated_at;

    @Column(nullable = false)
    private boolean deleted;
}
