package com.sambaonibus.samba_onibus.repository;

import com.sambaonibus.samba_onibus.domain.Motorista;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MotoristaRepository  extends JpaRepository<Motorista, Integer> {


}
