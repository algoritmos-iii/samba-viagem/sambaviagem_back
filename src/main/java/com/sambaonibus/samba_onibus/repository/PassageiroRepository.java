package com.sambaonibus.samba_onibus.repository;

import com.sambaonibus.samba_onibus.domain.Passageiro;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassageiroRepository extends JpaRepository<Passageiro, Integer> {
}
