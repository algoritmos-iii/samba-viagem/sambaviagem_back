package com.sambaonibus.samba_onibus.repository;

import com.sambaonibus.samba_onibus.domain.Viagem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViagensRepository extends JpaRepository<Viagem, Integer> {
}
